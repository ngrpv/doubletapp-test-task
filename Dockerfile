FROM python:3.11.3-slim-buster

ENV PYTHONBUFFERED=0
WORKDIR /app
RUN pip install pipenv
COPY Pipfile Pipfile.lock ./
RUN pipenv install --system

COPY ./src/ ./
