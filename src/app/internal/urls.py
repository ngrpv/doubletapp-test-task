from django.urls import path

from app.internal.transport.rest.views import UserView

urlpatterns = [
    path('me', UserView.as_view())
]
