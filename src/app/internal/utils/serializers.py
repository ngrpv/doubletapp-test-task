from app.internal.models.telegram_user import TelegramUser


def get_user_view(tg_user: TelegramUser) -> str:
    return f'Username: {tg_user.username}\n' \
           f'Phone number: {tg_user.user.phone_number}\n' \
           f'First name: {tg_user.user.first_name}\n' \
           f'Last name: {tg_user.user.last_name}'
