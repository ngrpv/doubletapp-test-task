import re

validate_phone_number_pattern = re.compile(r"^\+?[7-8]\d{10}$")


def is_correct_phone(text):
    return bool(validate_phone_number_pattern.search(text))
