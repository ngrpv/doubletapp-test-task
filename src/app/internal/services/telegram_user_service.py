from asgiref.sync import sync_to_async

from app.internal.DTO.create_user_dto import CreateUserDto
from app.internal.models.telegram_user import TelegramUser
from app.internal.services.user_service import UserService
from app.internal.utils.serializers import get_user_view


class TelegramUserService:
    def __init__(self):
        self._user_service = UserService()

    def get_by_chat_id(self, chat_id: int) -> TelegramUser:
        return TelegramUser.objects.get(chat_id=chat_id)

    def phone_number_is_filled(self, chat_id) -> bool:
        if not TelegramUser.objects.filter(chat_id=chat_id).exists():
            return False
        user = TelegramUser.objects.get(chat_id=chat_id).user
        return bool(user.phone_number)

    def get_user_profile_info(self, phone_number) -> str:
        user = self._user_service.get_by_phone_number(phone_number)
        tg_user = TelegramUser.objects.get(user=user)
        return get_user_view(tg_user)

    def get_user_profile_info_by_chat_id(self, chat_id) -> str:
        tg_user = TelegramUser.objects.get(chat_id=chat_id)
        return get_user_view(tg_user)

    def set_phone_number(self, chat_id, phone_number):
        tg_user = TelegramUser.objects.get(chat_id=chat_id)
        user = tg_user.user
        user.phone_number = phone_number
        user.save()

    async def create_user(self, create_user_dto: CreateUserDto):
        user = await sync_to_async(self._user_service.create_user)(create_user_dto)
        await TelegramUser(chat_id=create_user_dto.chat_id, username=create_user_dto.username, user=user).asave()

    async def user_exists(self, chat_id):
        return await TelegramUser.objects.filter(chat_id=chat_id).aexists()
