from app.internal.DTO.create_user_dto import CreateUserDto
from app.internal.models.user import User


class UserService:
    def __init__(self):
        pass

    def create_user(self, create_user_dto: CreateUserDto) -> User:
        user = User(first_name=create_user_dto.first_name,
                    last_name=create_user_dto.last_name)
        user.save()
        return user

    def get_by_id(self, id: int) -> User:
        return User.objects.get(id=id)

    def get_by_phone_number(self, phone_number) -> User:
        return User.objects.get(phone_number=phone_number)
