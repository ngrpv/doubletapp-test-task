from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.internal.models.telegram_user import TelegramUser


@admin.register(TelegramUser)
class TelegramUserAdmin(ModelAdmin):
    list_display = ('user', 'chat_id', 'username')
