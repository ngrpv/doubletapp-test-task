from django.contrib import admin
from django.contrib.admin import ModelAdmin

from app.internal.models.user import User


@admin.register(User)
class UserAdmin(ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone_number')
