class CreateUserDto:
    def __init__(self, first_name, last_name, chat_id, username):
        self.first_name = first_name
        self.last_name = last_name
        self.chat_id = chat_id
        self.username = username
