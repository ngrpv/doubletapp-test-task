import logging

from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, ContextTypes, MessageHandler, filters

from app.internal.transport.bot.commands import COMMAND
from app.internal.transport.bot.handlers import default, help_command, me, set_phone, start


async def _error_handler(update: object, context: ContextTypes.DEFAULT_TYPE) -> None:
    logging.error(msg="Exception while handling an update:", exc_info=context.error)
    if isinstance(update, Update):
        await context.bot.send_message(
            chat_id=update.effective_chat.id, text="Sorry, something went wrong"
        )


def _get_handlers():
    yield CommandHandler(str(COMMAND.ME), me)
    yield CommandHandler(str(COMMAND.SET_PHONE), set_phone)
    yield CommandHandler(str(COMMAND.START), start)
    yield CommandHandler(str(COMMAND.HELP), help_command)
    yield MessageHandler(filters.TEXT & (~filters.COMMAND), default)


class BotFactory:
    def __init__(self, token):
        self.token = token
        logging.basicConfig(
            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
            level=logging.INFO,
            filename='../../../logs/telegram.log',
            filemode='w'
        )
        self.logger = logging.getLogger(__name__)

    def get_bot(self):
        application = ApplicationBuilder().token(self.token).build()
        application.add_error_handler(_error_handler)

        for h in _get_handlers():
            application.add_handler(h)

        return application
