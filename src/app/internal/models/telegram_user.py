from django.db import models

from app.internal.models.user import User


class TelegramUser(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    chat_id = models.PositiveIntegerField()
    username = models.CharField(max_length=255, null=True)

    class Meta:
        verbose_name = 'TelegramUser'
