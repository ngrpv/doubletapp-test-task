from django.contrib.auth.models import AbstractUser
from django.db import models

from app.internal.models.user import User


class AdminUser(AbstractUser):
    class Meta:
        verbose_name = 'Admin'
