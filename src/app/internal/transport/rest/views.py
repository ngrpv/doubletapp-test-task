from django.http import HttpResponse, HttpResponseNotFound
from django.views import View

from app.internal.services.telegram_user_service import TelegramUserService
from app.internal.services.user_service import UserService


class UserView(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._telegram_service = TelegramUserService()
        self._user_service = UserService()

    def get(self, request, *args, **kwargs):
        if not request.GET.getlist('phone_number'):
            return HttpResponse(status=403)
        phone_number = request.GET.getlist('phone_number')[0]
        user = self._user_service.get_by_phone_number(phone_number)

        if user is None:
            return HttpResponseNotFound(f"User with phone number {phone_number} doesn't exists")
        user_view = self._telegram_service.get_user_profile_info(phone_number)
        return HttpResponse(user_view, content_type="application/json")
