from asgiref.sync import sync_to_async
from telegram import Update
from telegram.ext import ContextTypes

from app.internal.DTO.create_user_dto import CreateUserDto
from app.internal.services.telegram_user_service import TelegramUserService
from app.internal.transport.bot.user_replies import *
from app.internal.utils.validators import is_correct_phone

telegram_service = TelegramUserService()


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    chat_id = update.effective_chat.id
    if await telegram_service.user_exists(chat_id):
        await update.message.reply_text(GREETING_ALREADY_REGISTERED_USER)
        return

    await telegram_service.create_user(
        CreateUserDto(update.effective_user.first_name,
                      update.effective_user.last_name,
                      update.effective_chat.id,
                      update.effective_user.username))

    await update.message.reply_text(GREETING)


async def default(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text(CALL_HELP_TO_SEE_COMMANDS)


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text(HELP)


async def started(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_text(CALL_HELP_TO_SEE_COMMANDS)


async def set_phone(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if len(context.args) == 0:
        await update.message.reply_text(SET_PHONE_WITHOUT_PHONE_NUMBER)
        return
    phone_number = context.args[0]
    if not is_correct_phone(phone_number):
        await update.message.reply_text(INVALID_PHONE_NUMBER)
        return
    await sync_to_async(telegram_service.set_phone_number)(update.effective_chat.id, phone_number)
    await update.message.reply_text(PHONE_NUMBER_SET)


async def me(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if not await sync_to_async(telegram_service.phone_number_is_filled)(update.effective_chat.id):
        await update.message.reply_text(SET_PHONE_NUMBER_FIRST)
        return
    await update.message.reply_text(
        await sync_to_async(telegram_service.get_user_profile_info_by_chat_id)(update.effective_chat.id))
