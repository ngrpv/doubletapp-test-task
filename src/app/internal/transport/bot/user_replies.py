from app.internal.transport.bot.commands import COMMAND

GREETING = 'Hi! This bot can save your phone number and show if you need it'
SHOULD_SET_PHONE = f'Please set your phone using /{COMMAND.SET_PHONE} +79999999999(for example)'
CALL_HELP_TO_SEE_COMMANDS = f'You can send /{COMMAND.HELP} to see information about available commands'
INVALID_PHONE_NUMBER = 'Phone number is not correct. Try again'
PHONE_NUMBER_SET = f'Now you can get information about yourself by /{COMMAND.ME}'
GREETING_ALREADY_REGISTERED_USER = 'You have already started!'
SET_PHONE_WITHOUT_PHONE_NUMBER = f'Please print your phone number after /{COMMAND.SET_PHONE} command' \
                                 f'\nExample: "/{COMMAND.SET_PHONE} +79999999999"'
SET_PHONE_NUMBER_FIRST = f'Your phone number is not set. To fix it you can use /{COMMAND.SET_PHONE} command'

HELP = f'/{COMMAND.SET_PHONE} - set phone number\n' \
       f'/{COMMAND.ME} - get information about yourself\n' \
       f'/{COMMAND.HELP} - to get this message'
