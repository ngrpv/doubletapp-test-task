from enum import StrEnum


class COMMAND(StrEnum):
    SET_PHONE = 'set_phone'
    ME = 'me'
    START = 'start'
    HELP = 'help'
