from django.contrib import admin

# noinspection PyUnresolvedReferences
from app.internal.admin.admin_user import AdminUserAdmin
from app.internal.admin.telegram_user import TelegramUser

# noinspection PyUnresolvedReferences
from app.internal.admin.user import UserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
