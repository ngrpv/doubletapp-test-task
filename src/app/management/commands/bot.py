import os

from django.core.management import BaseCommand

from app.internal.bot import BotFactory


class Command(BaseCommand):
    help = 'Telegram Bot'

    def handle(self, *args, **options):
        bot = BotFactory(os.getenv('TOKEN')).get_bot()
        bot.run_polling()
